package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SwingTest {
	public void makeWindow() {
		JFrame frame = new JFrame("Testing");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		JLabel jlbempty = new JLabel("Test");
		jlbempty.setVerticalAlignment(SwingConstants.CENTER);
		jlbempty.setHorizontalAlignment(SwingConstants.CENTER);
		jlbempty.setPreferredSize(new Dimension(400, 300));
		frame.getContentPane().add(jlbempty, BorderLayout.CENTER);
		JButton button = new JButton("Click Here");
		button.setVerticalAlignment(SwingConstants.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void makeButton() {
		JFrame frame = new JFrame("Button Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton button = new JButton("Test");
		button.setVerticalAlignment(AbstractButton.CENTER);
		button.setHorizontalAlignment(AbstractButton.LEADING);
		frame.setPreferredSize(new Dimension(400, 300));
		frame.add(button);
		frame.getContentPane();
		frame.pack();
		frame.setVisible(true);
		button.setVisible(true);

	}

	public void calculatorLayout() {
		JFrame frame = new JFrame("Calculator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel bigPanel = new JPanel();
		bigPanel.setLayout(new GridLayout(2, 1, 10, 10));
		JPanel allButtonPanel = new JPanel();
		allButtonPanel.setLayout(new GridLayout(1,2,50,50));
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(4, 4, 8, 8));
		JButton b1 = new JButton("1");
		JButton b2 = new JButton("2");
		JButton b3 = new JButton("3");
		JButton b4 = new JButton("4");
		JButton b5 = new JButton("5");
		JButton b6 = new JButton("6");
		JButton b7 = new JButton("7");
		JButton b8 = new JButton("8");
		JButton b9 = new JButton("9");
		JButton b0 = new JButton("0");
		JButton period = new JButton(".");
		buttonPanel.add(b1);
		buttonPanel.add(b2);
		buttonPanel.add(b3);
		buttonPanel.add(b4);
		buttonPanel.add(b5);
		buttonPanel.add(b6);
		buttonPanel.add(b7);
		buttonPanel.add(b8);
		buttonPanel.add(b9);
		buttonPanel.add(b0);
		buttonPanel.add(period);
		JPanel opPanel = new JPanel();
		opPanel.setLayout(new GridLayout(4,4,8,8));
		JButton o1 = new JButton("AC");
		JButton o2 = new JButton("+");
		JButton o3 = new JButton("-");
		JButton o4 = new JButton("/");
		JButton o5 = new JButton("*");
		JButton o6 = new JButton("square");
		JButton o7 = new JButton("sqrt");
		JButton o8 = new JButton("ENTER");
		opPanel.add(o1);
		opPanel.add(o2);
		opPanel.add(o3);
		opPanel.add(o4);
		opPanel.add(o5);
		opPanel.add(o6);
		opPanel.add(o7);
		opPanel.add(o8);
		JTextField text = new JTextField();
		allButtonPanel.add(buttonPanel);
		allButtonPanel.add(opPanel);
		bigPanel.add(text);
		bigPanel.add(allButtonPanel);		
		frame.getContentPane();
		frame.add(bigPanel);
		frame.pack();
		frame.setVisible(true);
	}
}
